# -*- coding: utf-8 -*-

# parseCaf.py

##About the author:
# Denis Jacob Machado
# http://about.me/machadodj
# http://www.ib.usp.br/grant

##About parse.Caf.py:
# Parses padded CAF sequence files for DNA data
# CAF files must have only one contig per file
# Use -h or --help to see all the arguments available

##About the CAF format:
# CAF is a text format for describing sequence assemblies. It
# is acedb-compliant and is an extension of the ace-file
# format used earlier, but with support for base quality
# measures and a more extensive description of the Sequence
# data.

##About padded files:
# Padded means that gaps
# ("-") have been inserted where required in both contig and
# aligned readings so that there is a 1-1 correspondence
# between the aligned DNAs. In a Padded assembly there is
# exactly one Assembled_from line for each aligned read in a
# contig, and the DNA objects contain "-" padding characters.

##More about CAF formart at
# https://www.sanger.ac.uk/resources/software/caf/

#########################
# MODULES AND LIBRARIES #
#########################

try:import re
except ImportError:
	print("!ERROR: NOT FOUND: re")
	exit()
try:from sets import Set
except ImportError:
	print("!ERROR: NOT FOUND: Set in sets")
	exit()
try:import argparse
except ImportError:
	print("!ERROR: NOT FOUND: argparse")
	exit()

#############
# ARGUMENTS #
#############

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",help="increase output verbosity",action="store_true")
parser.add_argument("-c","--caf",help="sequences assemblies in Common Assembly Format (CAF)",metavar="<TEXT>")
parser.add_argument("-s","--start",help="start position (start from 1)",metavar="<INTEGER>",type=int)
parser.add_argument("-e","--end",help="end position",metavar="<INTEGER>",type=int)
parser.add_argument("-u","--unpadded",help="use if positions are from the unpadded sequence",action="store_true")
parser.add_argument("-r","--reads",help="report reads that align to the selected region",action="store_true")
parser.add_argument("-q","--quality",help="get mean quality score for selected region",action="store_true")
parser.add_argument("-x","--coverage",help="get mean coverage for selected region",action="store_true")
args=parser.parse_args()

###################
# GET CONTIG INFO #
###################

def contigInfo(CAF):
	print("Getting contig information...")
# get position of all reads (READSp)
	CONTIG=re.compile("Is_contig\s+Padded\s+(.+)\n\s*[^Assembled_from]",re.DOTALL).findall(CAF)
	try:CONTIG=CONTIG[0]
	except:
		print("!ERROR: Could not read contig information from %s"%(args.caf))
		exit()
	READSp={} # reads position
	reads=re.compile("Assembled_from\s+(.+)").findall(CONTIG)
	for read in reads:
		header,s1,s2,r1,r2=read.split()
		READSp[header]=[s1,s2,r1,r2]
# get contig DNA sequence (DNA)
	DNA=re.compile("Is_contig\s+Padded.+\n\s*DNA\s*:[^\n]+(.+)BaseQuality",re.DOTALL).findall(CAF)
	try:DNA=DNA[0]
	except:
		print("!ERROR: Could not read DNA information from %s"%(args.caf))
		exit()
	else:DNA=re.sub("\s","",DNA)
# get contig quality per base (QUALITY)
	QUALITY=re.compile("Is_contig\s+Padded.+BaseQuality[^\n]+([\s\d]+)",re.DOTALL).findall(CAF)
	try:QUALITY=QUALITY[0]
	except:
		print("!ERROR: Some error occured while readind data from %s"%(args.caf))
		exit()
	else:
		QUALITY=re.sub("\s+"," ",QUALITY)
		QUALITY=QUALITY.split()
	if(args.verbose):print("--contig was assembled from %d reads"%(len(READSp)))
	if(args.verbose):print("--contig size = %d"%(len(DNA)))
	fragmentInfo(CAF,READSp,DNA,QUALITY)

#####################
# GET FRAGMENT INFO #
#####################

def fragmentInfo(CAF,READSp,DNA,QUALITY):
# get selected padded DNA sequence (FRAGMENT)
	if(args.unpadded):print("Getting info for fragment s1=%d, s2=%d (skipping gaps)"%(args.start,args.end))
	else:print("Getting info for fragment s1=%d, s2=%d (counting gaps)"%(args.start,args.end))
	count=0 # counting skipping gaps
	padded=0 # count all bp
	COORD=[]
	FRAGMENT=""
	for bp in DNA:
		padded+=1
		if((args.unpadded)and((bp=="*")or(bp=="-"))):pass
		else:count+=1
		if((count>=args.start)and(count<=args.end)):
			if(count==args.start):COORD+=[padded]
			elif(count==args.end):COORD+=[padded]
			FRAGMENT+=bp
	if(args.verbose):print("--fragment selected:\n>from_%d_to_%d\n%s"%(args.start,args.end,FRAGMENT))
# get reads for that fragment (READSf)
	READSf={}
	for i in READSp:
		start=COORD[0]
		end=COORD[1]
		x=min(int(READSp[i][0]),int(READSp[i][1]))
		y=max(int(READSp[i][0]),int(READSp[i][1]))
		if((x>=start and x<=end)or(y>=start and y<=end)):READSf[i]=[x,y]
	if(args.verbose):print("--found %d read(s) for the selected fragment"%(len(READSf)))
# report reads that align to the selected region
	if(args.reads):
		print("\n<BEGIN TABLE>\n\n--reads aligned to the selected fragment\n\nheader\ts1\ts2\tQmin\tQmax\tQavg\tseq")
		for i in READSf:
			dna=re.compile("DNA\s*:\s*"+i+"(.+?)BaseQuality",re.DOTALL).findall(CAF)[0]
			dna=re.sub("\s","",dna)
			quality=re.compile("BaseQuality\s*:\s*"+i+"\s*([\s\d]+)",re.DOTALL).findall(CAF)[0]
			quality=re.sub("\s+"," ",quality)
			quality=[int(x) for x in quality.split()]
			print("%s\t%s\t%s\t%d\t%d\t%f\t%s"%(i,READSf[i][0],READSf[i][1],min(quality),max(quality),reduce(lambda x,y:x + y,quality)/float(len(quality)),dna))
		print("\n<END TABLE>\n")
# get quality scores for that fragment (QUALITYf)
	if(args.quality):
		print("Quality scores for the selected fragment\n(values collected from contig data)")
		QUALITYf=QUALITY[COORD[0]-1:COORD[1]]
		QUALITYf=[int(x) for x in QUALITYf]
		Qmin=min(QUALITYf)
		Qmax=max(QUALITYf)
		Qavg=reduce(lambda x,y:x + y,QUALITYf)/float(len(QUALITYf))
		Qseq=""
		for q in QUALITYf:Qseq+=str(q)+","
		Qseq=re.sub("\s","",Qseq)
		print("--Contig's quality scores: min=%d, max=%d, avg=%f\n--Quality sequence for fragment:\n%s"%(Qmin,Qmax,Qavg,Qseq[:len(Qseq)-1]))
# get fragment coverage
	if(args.coverage):coverage(COORD,READSf)

#########################
# GET FRAGMENT COVERAGE #
#########################

def coverage(COORD,READSf):
	print("\n<BEGIN TABLE>\n\n--coverage per position for selected fragment\n\nPosition\tCoverage")
	COVERAGE=[]
	for i in range(COORD[0],COORD[1]+1):
		coverage=0
		for j in READSf:
			if((int(READSf[j][0])<=i)and(int(READSf[j][1])>=i)):
				coverage+=1
		print("%d\t%d"%(i,coverage))
		COVERAGE+=[coverage]
	print("\n<END TABLE>\n")
	if(args.verbose):print("--min. coverage=%d, max. coverage=%d, avg. coverage=%f"%(min(COVERAGE),max(COVERAGE),reduce(lambda x,y:x + y,COVERAGE)/float(len(COVERAGE))))

##################
# INITIALIZATION #
##################

try:caf=open(args.caf,"rU")
except:
	print("!ERROR: NOT FOUND: %s"%(args.caf))
	exit()
else:
	print("Reading %s..."%(args.caf))
	CAF=caf.read()
	if(args.verbose):print("--%s was suscessfully read"%(args.caf))
	contigInfo(CAF)
	caf.close()
	
exit()