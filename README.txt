parseCaf - Copyright (C) 2014 - Denis Jacob Machado

parseCaf comes with ABSOLUTELY NO WARRANTY!

The program may be freely used, modified, and shared
under the GNU General Public License version 3.0
(GPL-3.0, http://opensource.org/licenses/GPL-3.0).

See LICENCE.txt for complete information about terms and
conditions

DESCRIPTION

CAF is a text format for describing sequence assemblies. It
is acedb-compliant and is an extension of the ace-file
format used earlier, but with support for base quality
measures and a more extensive description of the Sequence
data. parseCaf.py parses padded CAF sequence files for DNA
data. CAF files must have only one contig per file. Execute
"python parseCaf.py --help" to see all arguments available.